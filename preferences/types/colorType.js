const { Gdk } = imports.gi;

const ExtensionUtils = imports.misc.extensionUtils;
const SettingsType = ExtensionUtils.getCurrentExtension().imports.preferences.types.settingsType.SettingsType;

/* exported ColorType */
var ColorType = class extends SettingsType {
    get_widget() {
        return 'color-button';
    }

    get_signal() {
        return 'color-set';
    }

    get_value_from_widget(object) {
        return object.get_rgba().to_string();
    }

    update_widget(widget, setting_value) {
        const currentColor = new Gdk.RGBA();
        currentColor.parse(setting_value);
        widget.set_rgba(currentColor);
    }
};

