const ExtensionUtils = imports.misc.extensionUtils;
const Extension = ExtensionUtils.getCurrentExtension();
const Prefs = Extension.imports.preferences.libs.prefs.Prefs;

const schema = 'org.gnome.shell.extensions.TodoTxt';

/* exported init */
function init() {
    ExtensionUtils.initTranslations();
}

/* exported fillPreferencesWindow */
function fillPreferencesWindow(window) {
    const prefs = new Prefs(schema, window);
    window.search_enabled = true;
    prefs.fillPreferencesWindow(window);
}
/* vi: set expandtab tabstop=4 shiftwidth=4: */
